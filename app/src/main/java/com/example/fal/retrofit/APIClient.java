package com.example.fal.retrofit;

import android.support.annotation.NonNull;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Fal on 19/01/2018.
 */

public class APIClient {
    //TODO: URL à déporter dans les préférences...
    static final String BASE_URL = "http://henri-potier.xebia.fr/";
    static APIInterface client = null;

    // TODO: Peut être amélioré en passant le paramètre getObj par un évènement Otto...
    @NonNull
    public static APIInterface getClient() {
        Retrofit retrofit;
        if (client == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
            client = retrofit.create(APIInterface.class);
        }
        return client;
    }
}
