package com.example.fal.retrofit;

import com.example.fal.realm.Book;
import com.example.fal.realm.Offer;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Fal on 19/01/2018.
 */

public interface APIInterface {
    @GET("books")
    Observable<List<Book>> getBooks();
    @GET("books/{isbn_ids}/commercialOffers")
    Observable<Offer> getOffer(@Path("isbn_ids") String ids);
}
