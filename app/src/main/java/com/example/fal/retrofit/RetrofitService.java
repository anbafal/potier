package com.example.fal.retrofit;

import android.content.Context;

import com.example.fal.realm.RealmController;

import io.realm.Realm;

/**
 * Created by Fal on 19/01/2018.
 */

public class RetrofitService {
    Context context;
    Realm realm;

    //TODO: A déporter dans la classe Application pourquoi pas!
    public RetrofitService(Context context) {
        this.context = context;
        realm = RealmController.with(context).getRealm();
    }

}
