package com.example.fal.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.fal.R;
import com.example.fal.adapters.BucketRecyclerViewAdapter;
import com.example.fal.adapters.onRetrieveFromBucket;
import com.example.fal.realm.Bucket;
import com.example.fal.realm.Details;
import com.example.fal.realm.Offer;
import com.example.fal.realm.RealmController;
import com.example.fal.retrofit.APIClient;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmResults;

//TODO: Toutes les requêtes Realm pourraient être centralisées dans RealmController...
public class BucketListFragment extends Fragment {
    @BindView(R.id.recycler_view_bucket)
    RecyclerView recycler;
    @BindView(R.id.textViewInitialPrice)
    TextView tvInitialPrice;
    @BindView(R.id.textViewBookReduc)
    TextView tvBookReduc;
    @BindView(R.id.textViewCheckReduc)
    TextView tvcheckOutReduc;
    @BindView(R.id.textViewSliceReduc)
    TextView tvSliceReduc;
    @BindView(R.id.textViewFinalPrice)
    TextView tvFinalPrice;
    String defaultPrice;
    private Context fragmentContext;
    private Realm realm;
    private BucketRecyclerViewAdapter adapter;
    private LinearLayoutManager layoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentContext = getActivity().getApplicationContext();
        defaultPrice = fragmentContext.getString(R.string.default_price);
        realm = RealmController.with(fragmentContext).getRealm();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bucket_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            setUp();
            refreshHeaders();
        }
    }

    private void refreshHeaders() {
        String ids = "";
        RealmResults<Bucket> buckets = realm.where(Bucket.class).findAll();
        for (Bucket bucket : buckets) {
            ids = ids.concat(bucket.getBook().getIsbn() + ',');
        }
        if (!ids.equals("")) {
            Observable<Offer> observableOffer = APIClient.getClient().getOffer(ids.substring(0, ids.length() - 1));
            observableOffer.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(oOffers -> {
                        realm.executeTransactionAsync(realm1 -> {
                                    realm1.where(Offer.class).findAll().deleteAllFromRealm();
                                    realm1.copyToRealmOrUpdate(oOffers);
                                }
                        );
                    }, error -> {
                        System.out.println("TOAST");
                    }, () -> calculationOffer());
        } else {
            tvInitialPrice.setText(defaultPrice);
            tvBookReduc.setText(defaultPrice);
            tvcheckOutReduc.setText(defaultPrice);
            tvSliceReduc.setText(defaultPrice);
            tvFinalPrice.setText(defaultPrice);
        }
    }

    private void calculationOffer() {
        Float initialPrice = 0.f;
        RealmResults<Bucket> buckets = realm.where(Bucket.class).findAll();
        for (Bucket bucket : buckets) {
            initialPrice += bucket.getBook().getPrice() * bucket.getQuantity();
        }
        // à un instant t il n'est censé y avoir qu'une seul offre puisqu'il y a suppression des offres en local à chaque fois
        Map<String, Float> map = new HashMap<>();
        //TODO: Déporter la construction du map....
        for (Details detail : realm.where(Offer.class).findFirst().getDetails()) {
            switch (detail.getType()) {
                case "percentage":
                    map.put("BOOK_REDUC", initialPrice * detail.getValue() / 100);
                    break;
                case "minus":
                    map.put("CHECKOUT_REDUC", (float) detail.getValue());
                    break;
                case "slice":
                    map.put("SLICE_REDUC", (float) ((int)(initialPrice / detail.getSliceValue()) * detail.getValue()));
                    break;
            }
        }
        //TODO: mettre la recherche du max dans unne méthode externe....
        Map.Entry<String, Float> maxEntry = null;
        for (Map.Entry<String, Float> entry : map.entrySet()) {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
        }
        tvInitialPrice.setText(Float.toString(initialPrice));
        switch (maxEntry.getKey()) {
            case "BOOK_REDUC":
                tvBookReduc.setText(Float.toString(maxEntry.getValue()));
                tvcheckOutReduc.setText(defaultPrice);
                tvSliceReduc.setText(defaultPrice);
                break;
            case "CHECKOUT_REDUC":
                tvcheckOutReduc.setText(Float.toString(maxEntry.getValue()));
                tvBookReduc.setText(defaultPrice);
                tvSliceReduc.setText(defaultPrice);
                break;
            case "SLICE_REDUC":
                tvSliceReduc.setText(Float.toString(maxEntry.getValue()));
                tvBookReduc.setText(defaultPrice);
                tvcheckOutReduc.setText(defaultPrice);
                break;
        }
        tvFinalPrice.setText(Float.toString(initialPrice - maxEntry.getValue()));
    }

    private void setUp() {
        layoutManager = new LinearLayoutManager(fragmentContext);
        recycler.setHasFixedSize(true);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(layoutManager);
        onRetrieveFromBucket listener = (bucket) -> {
            realm.executeTransaction(realm1 -> {
                realm1.where(Bucket.class).equalTo("id", bucket.getId()).findFirst().deleteFromRealm();
                refreshHeaders();
            });
        };
        adapter = new BucketRecyclerViewAdapter(realm.where(Bucket.class).findAll(), listener);
        recycler.setAdapter(adapter);
    }


}
