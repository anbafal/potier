package com.example.fal.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.fal.R;
import com.example.fal.adapters.BookRecyclerViewAdapter;
import com.example.fal.adapters.onAddInBucket;
import com.example.fal.realm.Book;
import com.example.fal.realm.Bucket;
import com.example.fal.realm.RealmController;
import com.example.fal.retrofit.APIClient;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;


public class BookListFragment extends Fragment {

    @BindView(R.id.recycler_view_book)
    RecyclerView recycler;
    private Context fragmentContext;
    private Realm realm;
    private BookRecyclerViewAdapter adapter;
    private LinearLayoutManager layoutManager;

    public BookListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentContext = getActivity().getApplicationContext();
        realm = RealmController.with(fragmentContext).getRealm();
        setUp();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_book_list, container, false);
        ButterKnife.bind(this,view);
        layoutManager = new LinearLayoutManager(fragmentContext);
        recycler.setHasFixedSize(true);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(layoutManager);

        Observable<List<Book>> observableBooks = APIClient.getClient().getBooks();
        observableBooks.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(oBooks -> {
                    realm.executeTransactionAsync(realm1 -> realm1.copyToRealmOrUpdate(oBooks));
                },error -> {
                    System.out.println("TOAST");
                });

        recycler.setAdapter(adapter);
        return  view;
    }

    private void setUp() {
        onAddInBucket listener = (book) -> {
            realm.executeTransaction(realm1 -> {
                Bucket bucket = realm.where(Bucket.class).equalTo("book.isbn",book.getIsbn()).findFirst();
                if (bucket == null){
                    bucket = new Bucket();
                    bucket.setQuantity(1);
                    bucket.setBook(book);
                    realm1.copyToRealm(bucket);
                }
                else{
                    bucket.setQuantity(bucket.getQuantity()+1);
                    realm1.copyToRealmOrUpdate(bucket);
                }

            });

        };
        adapter = new BookRecyclerViewAdapter(realm.where(Book.class).findAll(),listener);
    }

}
