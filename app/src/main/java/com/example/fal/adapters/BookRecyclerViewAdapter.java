package com.example.fal.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fal.R;
import com.example.fal.realm.Book;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class BookRecyclerViewAdapter extends RealmRecyclerViewAdapter<Book, BookRecyclerViewAdapter.BookHolder> {

    private onAddInBucket listener;

    public BookRecyclerViewAdapter(OrderedRealmCollection<Book> data, onAddInBucket listener) {
        super(data, true);
        this.listener = listener;
    }

    @Override
    public BookHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View BookView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_book, parent, false);
        return new BookHolder(BookView,listener);
    }

    @Override
    public void onBindViewHolder(BookHolder holder, int position) {
        final Book obj = getItem(position);
        holder.data = obj;
        holder.title.setText(obj.getTitle());
        holder.price.setText(String.valueOf(obj.getPrice()));
        //TODO: Permettre lors du clic sur le synopsis de pouvoir voir le tout dans un Dialog Fragment...
        holder.synopsis.setText(obj.getSynopsis().get(0).substring(0, 100) + "...");
        Glide.with(holder.itemView.getContext())
                .load(obj.getCover())
                .into(holder.image);
    }

    class BookHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected ImageView image;
        protected ImageView imageAdd;
        protected TextView title;
        protected TextView price;
        protected TextView synopsis;
        protected Book data;
        protected onAddInBucket listener;

        public BookHolder(View view,onAddInBucket listener) {
            super(view);
            this.listener = listener;
            image = view.findViewById(R.id.imageView);
            imageAdd = view.findViewById(R.id.imageViewAdd);
            title = view.findViewById(R.id.textViewTitle);
            price = view.findViewById(R.id.textViewPrice);
            synopsis = view.findViewById(R.id.textViewSynopsis);
            imageAdd.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onAddInBucket(data);
        }
    }
}