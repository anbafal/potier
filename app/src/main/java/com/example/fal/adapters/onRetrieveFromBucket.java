package com.example.fal.adapters;

import com.example.fal.realm.Bucket;

public interface onRetrieveFromBucket {
    void onRetrieveFromBucket(Bucket bucket);
}