package com.example.fal.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fal.R;
import com.example.fal.realm.Book;
import com.example.fal.realm.Bucket;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;


public class BucketRecyclerViewAdapter extends RealmRecyclerViewAdapter<Bucket, BucketRecyclerViewAdapter.BucketHolder> {

    private onRetrieveFromBucket listener;

    public BucketRecyclerViewAdapter(OrderedRealmCollection<Bucket> data, onRetrieveFromBucket listener) {
        super(data, true);
        this.listener = listener;
    }

    @Override
    public BucketHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View BucketView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_bucket, parent, false);
        return new BucketHolder(BucketView,listener);
    }

    @Override
    public void onBindViewHolder(BucketHolder holder, int position) {
        final Bucket obj = getItem(position);
        final Book book = getItem(position).getBook();
        holder.data = obj;
        holder.title.setText(book.getTitle());
        holder.priceUnit.setText(String.valueOf(book.getPrice()));
        holder.quantity.setText(String.valueOf(obj.getQuantity()));
        holder.totalPriceItem.setText(String.valueOf(book.getPrice()*obj.getQuantity()));
        Glide.with(holder.itemView.getContext())
                .load(book.getCover())
                .into(holder.image);
    }


    class BucketHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected ImageView image;
        protected ImageView imageDelete;
        protected TextView title;
        protected TextView quantity;
        protected TextView priceUnit;
        protected TextView totalPriceItem;
        protected Bucket data;
        protected onRetrieveFromBucket listener;

        public BucketHolder(View view,onRetrieveFromBucket listener) {
            super(view);
            this.listener = listener;
            image = view.findViewById(R.id.imageViewBucket);
            imageDelete = view.findViewById(R.id.imageViewBucketDelete);
            title = view.findViewById(R.id.textViewTitleBucket);
            quantity = view.findViewById(R.id.textViewQuantity);
            priceUnit = view.findViewById(R.id.textViewPriceBucket);
            totalPriceItem = view.findViewById(R.id.textViewPriceItem);
            imageDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onRetrieveFromBucket(data);
        }
    }
}