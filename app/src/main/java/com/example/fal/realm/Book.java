package com.example.fal.realm;


import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Fal on 19/01/2018.
 */

public class Book extends RealmObject {
    @PrimaryKey
    private String isbn;
    private String title;
    private String cover;
    private int  price;
    private RealmList<String> synopsis;

    public Book() {
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public RealmList<String> getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(RealmList<String> synopsis) {
        this.synopsis = synopsis;
    }
}
