package com.example.fal.realm;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by nakadiri on 27/01/2018.
 */

public class Details  extends RealmObject{
    @PrimaryKey
    private String id;
    private String type;
    private Integer value;
    private Integer sliceValue;

    public Details() {
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getSliceValue() {
        return sliceValue;
    }

    public void setSliceValue(Integer sliceValue) {
        this.sliceValue = sliceValue;
    }
}
