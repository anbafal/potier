package com.example.fal.realm;

import com.google.gson.annotations.SerializedName;

import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Fal on 19/01/2018.
 */

public class Offer extends RealmObject {
    @PrimaryKey
    private String id;
    @SerializedName("offers")
    private RealmList<Details> details;

    public Offer() {
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public RealmList<Details> getDetails() {
        return details;
    }

    public void setDetails(RealmList<Details> details) {
        this.details = details;
    }
}


