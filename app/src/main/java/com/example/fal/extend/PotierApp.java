package com.example.fal.extend;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.example.fal.retrofit.RetrofitService;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Fal on 19/01/2018.
 */

public class PotierApp extends Application {
    private static RetrofitService retrofitService;


    public static RetrofitService getRetrofitService() {
        return retrofitService;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

        retrofitService = new RetrofitService(getApplicationContext());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
